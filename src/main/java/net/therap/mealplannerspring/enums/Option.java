package net.therap.mealplannerspring.enums;

/**
 * @author pranjal.chakraborty
 * @since 5/11/17
 */
public enum Option {
    ADD_ITEM, ADD_PLAN, VIEW_PLAN, VIEW_ITEMS
}
