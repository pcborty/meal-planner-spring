package net.therap.mealplannerspring.enums;

/**
 * @author pranjal.chakraborty
 * @since 5/10/17
 */
public enum Type {
    BREAKFAST, LUNCH
}
