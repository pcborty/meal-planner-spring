package net.therap.mealplannerspring.helper;

public interface Constants {

    String FULL_NAME = "fullname";
    String USER_NAME = "uname";
    String PASSWORD = "pass";
    String ITEM_NAME = "item_name";
    String ADD_ITEM_NOTIFY = "messageAddItem";
    String ADD_PLAN_NOTIFY = "messageAddPlan";
    String FAILURE_NOTIFY = "failureMessage";

    String HOME_PATH = "home";
    String ABS_HOME_PATH = "WEB-INF/views/homepage.jsp";
    String REGISTER_PATH = "register";
    String ABS_REGISTER_PATH = "/WEB-INF/views/register.jsp";
    String LOGIN_PATH = "login";
    String ABS_LOGIN_PATH = "/WEB-INF/views/index.jsp";
    String ABS_VIEW_PLANS_PATH = "/WEB-INF/views/viewPlans.jsp";
    String ABS_VIEW_ITEMS_PATH = "/WEB-INF/views/viewItems.jsp";
    String ADD_PLAN_PATH = "addPlan";
    String ABS_ADD_PLAN_PATH = "/WEB-INF/views/addPlans.jsp";
    String ADD_ITEM_PATH = "addItem";
    String ABS_ADD_ITEM_PATH = "/WEB-INF/views/addNewItem.jsp";
    String ASSETS_PATH = "/assets";

    String ITEM_LIST = "items";
    String MEAL_LIST = "meals";
}
